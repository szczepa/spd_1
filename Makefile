TARGET_NAME = superprogram

TARGET = main

SRCDIR = src
INCDIR = inc
OBJDIR = obj

# Zrodla
SRC = $(wildcard $(SRCDIR)/*.cpp)
OBJ = $(patsubst %.cpp, %.o, $(subst $(SRCDIR),$(OBJDIR),$(SRC)))
HDR = $(wildcard $(INCDIR)/*.hh)
CPP = c++

# Flagi kompilacji

CPPFLAGS = -pthread -std=c++14 -I$(INCDIR)
CPPFLAGS += -Wall -pedantic -g  

#========================| Tester |===========================

TESTER_NAME = tester

TESTER_TARGET = test_main

TESTER_DIR = tests

TESTER_OBJ = $(subst $(OBJDIR)/main.o,,$(OBJ))

TESTER_LDFLAGS = -lgtest -lgtest_main -pthread

#==============================================================

SPD: build_msg dir $(OBJ)
	$(CPP) -o $(TARGET_NAME) $(OBJ)
	
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CPP) -c -o $@ $< $(CPPFLAGS)

build_test: $(TESTER_OBJ)
	@echo "Building test: $(TESTER_NAME)"
	$(CPP) $(CPPFLAGS) $(TESTER_LDFLAGS)\
		$(TESTER_DIR)/$(TESTER_TARGET).cpp $(TESTER_OBJ) \
		-o $(TESTER_NAME)
	
test: build_test
	@echo "Running test: $(TESTER_NAME)"
	@./$(TESTER_NAME)

dir:
	@if [ ! -d "$(OBJDIR)" ]; then mkdir $(OBJDIR);\
		@echo "Preparing:";\
	fi

run: SPD 
	@echo "Running: $(TARGET_NAME)"
	@./$(TARGET_NAME)

run_full: SPD 
	@echo "Running: $(TARGET_NAME)"
	@time ./$(TARGET_NAME) < data
	@echo " "
	@cat wynik.txt

clean_msg:
	@echo "Cleaning:"

build_msg:
	@echo "Building:"


clean: clean_msg
	rm -f $(OBJDIR)/* $(TARGET_NAME) $(TESTER_NAME)
	rm -rf $(TESTER_NAME).dSYM
	rm -rf info.txt wynik.txt


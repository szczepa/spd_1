#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <string>
#include <fstream>
#include <thread>
#include <future>
#include <sstream>


#include "Brick.hh"
#include "Komparatory.hh"
#include "Algorytmy.hh"
#include "Randomize.hh"

Randomize *Randomize::_instance = 0;



int main()
{
unsigned int suma = 0;
	
	std::fstream sequence;
	std::stringstream fs;
	sequence.open ("wynik.txt", std::fstream::in | std::fstream::out | std::fstream::trunc);

	fs << std::endl <<  "Dlugosc uszeregowania:" << std::endl;

	for(int i = 0; i < 4; ++i)
	{	
		int N;
		int m = 0;
		std::string nazwa_serii;
		std::vector<sequenceNtime> wszystkie; 


		std::cin >> nazwa_serii;
		std::cin >> N;

		std::vector<Brick*> vec;
		vec.reserve(N);

		for(int i = 0; i < N; ++i)
		{
			Brick * b = new Brick;       
			b->setNr(i);   
			vec.push_back(b);
		}
		
					
		Randomize::instance()->setValue(0);
		wszystkie.emplace_back(sequenceNtime(Algorytm1(vec),0));
		Randomize::instance()->setValue(1);
		wszystkie.emplace_back(sequenceNtime(Algorytm1(vec),0));
		Randomize::instance()->setValue(2);
		wszystkie.emplace_back(sequenceNtime(Algorytm1(vec),0));


		for(auto& it : wszystkie)
		{
			++m;
			it.second = SumaCzasu(it.first, 0, it.first.size());
			
		}

		std::sort(wszystkie.begin(),wszystkie.end(),byTime());
		
		suma += wszystkie.at(0).second;

		sequence << nazwa_serii << '\n';
		std::for_each(wszystkie.at(0).first.begin(), wszystkie.at(0).first.end(),
			[&sequence](Brick * it){sequence << it->getNr() << " " ;});
		sequence << '\n';


		fs << nazwa_serii << ": " << wszystkie.at(0).second << std::endl;
		
	}
	fs << "Suma : " << suma << std::endl;
	
	sequence.close();
	std::cout << fs.str();
	std::cout << "\033[1;31m OCENA \033[0m :  " << WystawOcene(suma) << std::endl;
	std::cout << "INFO: Uszeregowania znajduja sie w pliku wynik.txt" << std::endl;
	return 0;

}

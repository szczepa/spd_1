#include "Komparatory.hh"

bool RLess_than::operator() (const Brick* brck1, const Brick* brck2) {
	return brck1->getR() < brck2->getR();
}

bool QLess_than::operator() (const Brick* brck1, const Brick* brck2) {
	return brck1->getQ() < brck2->getQ();
}

bool TimeLess_than::operator() (const Brick* brck1, const Brick* brck2) {
	return brck1->getTime() < brck2->getTime();
}

bool QuotLess_than::operator() ( const Brick* brck1, const Brick* brck2) { 
	return brck1->getQuot() > brck2->getQuot();
}

bool SumLess_than::operator() ( const Brick* brck1, const Brick* brck2) { 
	return brck1->getSum() < brck2->getSum();
}

bool byTime::operator() ( const sequenceNtime seq1, const sequenceNtime seq2) { 
	return seq1.second < seq2.second;
}

bool Comp::operator() (const unsigned int& tim, const unsigned int& tim2) {
	 if ( Randomize::instance()->getValue() == 2) 
		 return tim <= tim2;
	 else return tim < tim2;
}

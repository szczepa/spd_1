#include "Algorytmy.hh"


std::vector<Brick*> Algorytm2( std::vector<Brick*> VectOfBricks )  {

	RLess_than Comparator;

	sort(VectOfBricks.begin(), VectOfBricks.end(), Comparator);


	return VectOfBricks;
}

void RandomizeVector(std::vector<Brick*>& Vec) {
	unsigned int k, j;	
	std::default_random_engine gen;
	std::uniform_int_distribution<int> dist(0,Vec.size()-1);	
	
	for ( unsigned int i = 0; i < Vec.size(); ++i ) {
		k = dist(gen);
		j = dist(gen);
		std::swap(Vec.at(k), Vec.at(j) );
	}

}


std::vector<Brick*> Algorytm1( std::vector<Brick*> VectOfBricks )  {

	std::vector<Brick*> vec;
	Comp Compare;
	unsigned int time1,time2, j = 0;
	vec.reserve(VectOfBricks.size());
	
 	if (Randomize::instance()->getValue() == 0)	RandomizeVector(VectOfBricks);	
	else 
	sort(VectOfBricks.begin(),VectOfBricks.end(), SumLess_than());

	vec.push_back(VectOfBricks.front());

	for ( unsigned int i = 1; i < VectOfBricks.size(); ++i){
		vec.insert(vec.begin(),VectOfBricks.at(i));
		j = 0;
		while ( j < i-1 )	{

			time1 =  SumaCzasu(vec,0,i+1);
			std::swap(vec.at(j),vec.at(j+1));
			time2 =  SumaCzasu(vec,0,i+1);
		
			if ( Compare(time1,time2) ) 
				std::swap(vec.at(j),vec.at(j+1));
			++j;
		}

	}

	return vec;

}

std::vector<Brick*> Algorytm3( std::vector<Brick*>& VectOfBricks )  {

	TimeLess_than Comparator;
	unsigned long hlp = 0;
	std::vector<Brick*> vct;
	vct.reserve(VectOfBricks.size());

	sort(VectOfBricks.begin(), VectOfBricks.end(), Comparator);

	vct.push_back(VectOfBricks.back());
	
	for( unsigned int i = 0;i < VectOfBricks.size()-1; ++i) {
		
		if ( hlp < VectOfBricks.back()->getR() ) {
			hlp += VectOfBricks.at(i)->getTime();
			vct.insert(vct.begin()+i,VectOfBricks.at(i));
		} else {
			vct.push_back(VectOfBricks.at(i));
		}
	}

	return vct;
}

std::vector<Brick*> Algorytm4( std::vector<Brick*>& VectOfBricks )  {
	
	std::deque<Brick*> DeqOfBricks;
	auto rsorted = VectOfBricks;
	auto tsorted = VectOfBricks;

	std::sort(rsorted.begin(),rsorted.end(), RLess_than());
	std::sort(tsorted.begin(),tsorted.end(), TimeLess_than());

	
	while(DeqOfBricks.size() < VectOfBricks.size())
	{
		size_t i = 0;
		DeqOfBricks.push_front(rsorted.back());
		rsorted.pop_back();

		unsigned int suma = 0;

		while (suma < DeqOfBricks.at(i)->getR() + DeqOfBricks.at(i)->getTime())
		{
			DeqOfBricks.push_front(rsorted.at(0));
			suma += DeqOfBricks.at(i)->getR() + DeqOfBricks.at(i)->getTime();
		}


	}

	std::vector<Brick*> ans(DeqOfBricks.begin(),DeqOfBricks.end()); 
	
	
	return ans;
}

unsigned int SumaCzasu(const std::vector<Brick*>& bricks,const unsigned int& from, const unsigned int& to)
{
	unsigned int suma = 0;
	unsigned int tail = 0;
	unsigned int help = 0;

	if ( (from != to) && bricks.size() ) {

	suma = bricks.at(0)->getR(); // czas dostarczenia pierwszego elementu

	for(size_t i = from; i < to; ++i)
	{
		suma += bricks.at(i)->getP(); // czas wykonania oraz dostarczenia i elementow
		help = suma + bricks.at(i)->getQ();// suma czasow wykonania oraz dostarczenia i elementow
													  // i ostatni czas stygniecia
		if ( tail < help ) tail = help;
		
		if(i < to-1)
		{	

			if( suma < bricks.at(i+1)->getR() ){
				suma += bricks.at(i+1)->getR() - suma;	
			}
		}
	}

	}
	
	return tail;
}

double WystawOcene(unsigned int & suma)
{
	/*
ocena        2.5     3.0     3.5     4.0     4.5     5.0     5.5
suma:     168306  127362  103378  100926  100381  100010  100000
*/
	double ocena = 2;

	
	if(suma > 168306){return ocena;}
	if(suma <= 168306){ocena = 2.5;}
	if(suma <= 127362){ocena = 3;}
	if(suma <= 103378){ocena = 3.5;}
	if(suma <= 100926){ocena = 4;}
	if(suma <= 100381){ocena = 4.5;}
	if(suma <= 100010){ocena = 5;}
	if(suma <= 100000){ocena = 5.5;}
	return ocena;	
}

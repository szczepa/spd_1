#ifndef ALGOR_HH
#define ALGOR_HH

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <deque>
#include <algorithm>
#include <random>
#include "Randomize.hh"
#include "Brick.hh"
#include "Komparatory.hh"

extern unsigned int randomize;

std::vector<Brick*> Algorytm2( std::vector<Brick*> VectOfBricks );
std::vector<Brick*> Algorytm1( std::vector<Brick*> VectOfBricks );
std::vector<Brick*> Algorytm3( std::vector<Brick*>& VectOfBricks );
void RandomizeVector(std::vector<Brick*>& Vec);
std::vector<Brick*> Algorytm4( std::vector<Brick*>& VectOfBricks );

unsigned int SumaCzasu(const std::vector<Brick*>& bricks,const unsigned int& from, const unsigned int& to);
double WystawOcene(unsigned int & suma);

#endif

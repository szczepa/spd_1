#ifndef SING_HH
#define SING_HH
#include <iostream>

class Randomize {
	private:
		Randomize( unsigned int v = 0) { val = v; } 
		unsigned int val;
		static Randomize *_instance;
	public:
		const unsigned int& getValue() const { return val; }
		void setValue(unsigned int v) { val = v; }
		static Randomize* instance() {
			if ( !_instance)
				_instance = new Randomize;
			return _instance;
		}


};


#endif


#ifndef BRICK_HH
#define BRICK_HH

#include <iostream>


class Brick {
	
	unsigned int R,P,Q;
	float Quot;
	unsigned int nr;

	public:
	const unsigned int& getR() const { return R; }
	const unsigned int& getP() const { return P; }
	const unsigned int& getQ() const { return Q; }
	const unsigned int  getSum() const { return Q+R; }
	const unsigned int  getDiff() const { return Q-R;}
	const unsigned int  getTime() const { return R+P;}
	const float&		  getQuot() const { return	Quot;}
	const unsigned int& getNr() const { return nr; }

	void setR( unsigned int& val ) { R = val; }
	void setP( unsigned int& val ) { P = val; }
	void setQ( unsigned int& val ) { Q = val; }
	void setQuot( const float& val ) { Quot = val; }
	void setNr( unsigned int  val ) {++val; nr = val; }
	 

	void Print()
	{
		std::cout << " | " << R << " - - - " << P << " - - - " << Q << " | \n"; 
	}

	Brick(unsigned int Rval, unsigned int Pval, unsigned int Qval) : R{Rval}, P{Pval}, Q{Qval} {}
	Brick()
	{
		std::cin >> R;
		std::cin >> P;
		std::cin >> Q;

		nr++;

		if ( !(R||Q) )	 Quot = -1;
		else Quot = R/Q;
	}


};

#endif

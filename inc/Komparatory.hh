#ifndef KOMP_HH
#define KOMP_HH
#include <iostream>
#include <vector>
#include <utility>
#include "Brick.hh"
#include "Randomize.hh"


typedef std::pair<std::vector<Brick*>,unsigned int> sequenceNtime;

class RLess_than {
	public:
		bool operator() (const Brick*, const Brick*);
};

class QLess_than {
	public:
		bool operator() (const Brick*, const Brick*);
};

class TimeLess_than { 
	public:
		bool operator() (const Brick*, const Brick*);
};

class QuotLess_than { 
	public:
		bool operator() (const Brick*, const Brick*);
};

class byTime { 
	public:
		bool operator() (const sequenceNtime, const sequenceNtime);
};

class SumLess_than { 
	public:
		bool operator() (const Brick*, const Brick*);
};

class Comp { 
	public:
		bool operator() (const unsigned int& tim, const unsigned int& tim2);
};

#endif

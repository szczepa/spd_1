#include <iostream>
#include <gtest/gtest.h>

#include "../inc/Algorytmy.hh"
#include "../inc/Brick.hh"
#include "../inc/Komparatory.hh"

using namespace  std;


//g++ main.cpp -lgtest -lgtest_main -pthread

class TEST_Algorytm: public testing::Test{
public:


	void SetUp()
	{
		std::cin >> nazwa_serii;
		std::cin >> N;


		vec.reserve(N);

		for(int i = 0; i < N; ++i)
		{
			Brick * b = new Brick;
			vec.push_back(b);
		}
	}

	void TearDown()
	{
		std::cout<<"Test przeprowadzono dla danych o nazwie ::" << nazwa_serii << '\n';

	}

protected:
	int N;
	std::string nazwa_serii;
	std::vector<Brick*> vec;
}; 

TEST_F (TEST_Algorytm, Test_rozmiaru1) 
{ 
	auto ans = Algorytm1(vec);
	EXPECT_EQ(N,ans.size());
}

TEST_F (TEST_Algorytm, Test_rozmiaru2) 
{ 
	auto ans = Algorytm2(vec);
	EXPECT_EQ(N,ans.size());
}

TEST_F (TEST_Algorytm, Test_rozmiaru3) 
{ 
	auto ans = Algorytm3(vec);
	EXPECT_EQ(N,ans.size());
}

TEST_F (TEST_Algorytm, Test_rozmiaru4) 
{ 
	auto ans = Algorytm4(vec);
	EXPECT_EQ(N,ans.size());
}

int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
